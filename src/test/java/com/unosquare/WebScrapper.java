package com.unosquare;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;

import java.io.IOException;
import java.util.Optional;

@Slf4j
public class WebScrapper {


    @Test
    public void testExpression() throws IOException {

        var document = Jsoup.connect("https://www.google.com/search?tbs=lf:1,lf_ui:9&tbm=lcl&q=restaurantes+abiertos+cerca+de+mi").get();
        Elements links = document.select(".rllt__details");

        links.stream().forEach(items -> {

            var nombreRestaurante = items.getElementsByClass("OSrXXb").stream()
                    .filter(entry -> StringUtils.isNotBlank(entry.text()))
                    .findFirst()
                    .get().text();


            var calificacion = items.getElementsByClass("Y0A0hc").stream()
                    .filter(entry -> StringUtils.isNotBlank(entry.text()))
                    .findFirst()
                    .get()
                    .getElementsByClass("yi40Hd YrbPuc")
                    .stream().findFirst()
                    .get().text();

            var horario = Optional.ofNullable( items.getElementsByTag("div")
                    .stream()
                    .filter(entry -> entry.text().trim().length() == 11)
                    .filter(entry -> entry.text().contains(":"))
                    .findFirst()
                    .orElseGet(() -> null));

            log.info("Restaurante: {}, calificacion: {}, horario: {} ", nombreRestaurante, calificacion,
                    horario.isEmpty() ? "" : horario.get().text());

        });
    }
}
